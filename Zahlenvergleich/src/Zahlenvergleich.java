import java.util.Scanner;

public class Zahlenvergleich {

	public static void main(String[] args) {

	//Deklaration der Variablen
	int zahl1, zahl2;
	
	//Initialisierung eines Scanner-Objektes
	Scanner tastatur = new Scanner(System.in);
	
	//Eingabe
	System.out.print("Bitte geben Sie die erste Zahl ein: ");
	zahl1 = tastatur.nextInt();
	System.out.print("Bitte geben Sie die zweite Zahl ein: ");
	zahl2 = tastatur.nextInt();
	
	//Verarbeitung
	if(zahl1 < zahl2) {
		System.out.println("Die erste Zahl ist kleiner als die zweite Zahl.");
	}else if(zahl1 > zahl2) {
		System.out.println("Die erste Zahl ist gr��er als die zweite Zahl.");
	}else {
		System.out.println("Beide Zahlen sind gleich gro�.");		
	}
	}

}


