import java.util.Scanner;

public class SwitchCaseNoten {


	public static void main(String[] args) {
		
		programmhinweis();
		int zahl1 = eingabe("Bitte geben Sie Ihre Note ein: ");
		System.out.println();
		verarbeitungAusgabe(zahl1);

	}
			
			static void programmhinweis() {
				System.out.println("Dieses Programm gibt die sprachliche Umschreibung der Noten 1 bis 6 aus.");
				System.out.println();
				}
			static int eingabe(String text) {
				Scanner sc = new Scanner(System.in);
				System.out.print(text);
				int zahl1 = sc.nextInt();
				return zahl1;
			}
			static void verarbeitungAusgabe(int zahl1) {
				switch(zahl1) {
				
				case 1:
					System.out.println("Sehr gut!");
				break;
				
				case 2:
					System.out.println("Gut!");
				break;
				
				case 3:
					System.out.println("Befriedigend!");
				break;
				
				case 4:
					System.out.println("Ausreichend!");
				break;
				
				case 5:
					System.out.println("Mangelhaft!");
				break;
				
				case 6:
					System.out.println("Ungenügend!");
				break;
				
				default: 
					System.out.println("Es gibt nur die Noten 1-6:");
					System.out.println("==========================");
					System.out.println();
					System.out.println("- Note 1: Sehr gut     -");
					System.out.println("- Note 2: Gut          -");
					System.out.println("- Note 3: Befriedigend -");
					System.out.println("- Note 4: Ausreichend  -");
					System.out.println("- Note 5: Mangelhaft   -");
					System.out.println("- Note 6: Ungenügend   -");
				
				}
					
			}
			
}
