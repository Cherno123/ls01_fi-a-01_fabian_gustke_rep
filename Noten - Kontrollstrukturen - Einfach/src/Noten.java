import java.util.Scanner;

public class Noten {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

	programmhinweis();
	int zahl1 = eingabe("Bitte geben Sie Ihre Note ein: ");
	System.out.println();
	int ergebnis = verarbeitungAusgabe(zahl1);

	}
	
	static void programmhinweis() {
		System.out.println("Dieses Programm gibt die sprachliche Umschreibung der Noten 1 bis 6 aus.");
		System.out.println();
		}
	static int eingabe(String text) {
		System.out.print(text);
		int zahl1 = sc.nextInt();
		return zahl1;
	}
	static int verarbeitungAusgabe(int zahl1) {
		if(zahl1 == 1) {
			System.out.println("Sehr gut!");
		}else if(zahl1 == 2) {
			System.out.println("Gut!");
		}else if(zahl1 == 3) {
			System.out.println("Befriedigend!");
		}else if(zahl1 == 4) {
			System.out.println("Ausreichend!");
		}else if(zahl1 == 5) {
			System.out.println("Mangelhaft!");
		}else if(zahl1 == 6) {
			System.out.println("Ungenügend!");
		}else {
			System.out.println("Es gibt nur die Noten 1-6:");
			System.out.println("==========================");
			System.out.println();
			System.out.println("- Note 1: Sehr gut     -");
			System.out.println("- Note 2: Gut          -");
			System.out.println("- Note 3: Befriedigend -");
			System.out.println("- Note 4: Ausreichend  -");
			System.out.println("- Note 5: Mangelhaft   -");
			System.out.println("- Note 6: Ungenügend   -");
		}
		return zahl1;
	}
	

}
