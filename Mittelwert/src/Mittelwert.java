import java.util.Scanner;

public class Mittelwert {
	
  static Scanner myScanner = new Scanner(System.in);

   public static void main(String[] args) {

	   programmhinweis("Geben Sie bitte 2 Zahlen ein. Danach wird der Mittelwert berechnet.");
	   double zahl1 = eingabeZahlen("Zahl 1: ");
	   System.out.println();
	   double zahl2 = eingabeZahlen("Zahl 2: ");
	   double ergebnis = verarbeitungZahlen(zahl1, zahl2);
	   ausgabeZahlen(zahl1, zahl2, ergebnis);
      
   }
   
   static void programmhinweis(String text) {
	System.out.println(text);
	System.out.println("=============================");
	System.out.println();
   }
   
   static double eingabeZahlen(String text) {
	   System.out.print(text);
	   double zahl = myScanner.nextDouble();
	   return zahl;
   }
   
   static double verarbeitungZahlen(double zahl1, double zahl2) {
	   return (zahl1 + zahl2) / 2; 
   }
   
   static void ausgabeZahlen(double zahl1, double zahl2, double ergebnis) {
	 System.out.println();
	   System.out.printf("Mittelwertberechnung: (%.1f + %.1f) / 2 = %.2f", zahl1, zahl2, ergebnis);  
   }
}
