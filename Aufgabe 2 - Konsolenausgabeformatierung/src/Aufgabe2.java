
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
// Fakult�t 0:
		System.out.printf("0!%4s", "=");
		System.out.printf("%20s", "=");
		System.out.printf("%5s %n", "1");
// Fakult�t 1:		
		System.out.printf("1!%6s", "= 1");
		System.out.printf("%18s", "=");
		System.out.printf("%5s %n", "1");
// Fakult�t 2:
		System.out.printf("2!%10s", "= 1 * 2");
		System.out.printf("%14s", "=");
		System.out.printf("%5s %n", "2");
// Fakult�t 3:		
		System.out.printf("3!%14s", "= 1 * 2 * 3");
		System.out.printf("%10s", "=");
		System.out.printf("%5s %n", "6");
// Fakult�t 4:		
		System.out.printf("4!%18s", "= 1 * 2 * 3 * 4");
		System.out.printf("%6s", "=");
		System.out.printf("%5s %n", "24");
// Fakult�t 5:	
		System.out.printf("5!%22s", "= 1 * 2 * 3 * 4 * 5");
		System.out.printf("%2s", "=");
		System.out.printf("%5s %n", "120");
	}

}
