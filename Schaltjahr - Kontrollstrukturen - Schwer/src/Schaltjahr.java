import java.util.Scanner;

public class Schaltjahr {
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		programmhinweis();
		int jahr = eingabe("Geben Sie das Jahr ein: ");
		System.out.println();
		int erg = verarbeitungAusgabe(jahr);
		
	}
	
	static void programmhinweis() {
		System.out.println("Dieses Programm erkennt, ob das eingegebene Jahr ein Schaltjahr ist.");
		System.out.println();
	}
	
	static int eingabe(String text) {
		System.out.print(text);
		int jahr = sc.nextInt();
		
		return jahr;
	}
	
	static int verarbeitungAusgabe(int jahr) {
		if (jahr % 4 == 0) {
			if (jahr > 1582) {
				if (jahr % 100 == 0) {
					if (jahr % 400 == 0) {
						System.out.println("Es handelt sich um ein Schaltjahr");
					} 
					else {
						System.out.println("Es handelt sich um kein Schaltjahr");
					}
				} 
				else {
					System.out.println("Es handelt sich um ein Schaltjahr");
				}
			}
		} 
		else {
			System.out.println("Es handelt sich um kein Schaltjahr");
		}
		
		return jahr;
			
		}
	
	}
