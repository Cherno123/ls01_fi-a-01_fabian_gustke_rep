
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Fahrenheit/Celsius
				System.out.printf("Fahrenheit%3s", "|");
				System.out.printf("%10s %n", "Celsius");
		// Temperatur 1:		
				System.out.printf("------------------------ %n");
		// Temperatur 2:
				System.out.printf("-20%10s", "|");
				System.out.printf("%10.2f %n", -28.8889);
		// Temperatur 3:		
				System.out.printf("-10%10s", "|");
				System.out.printf("%10.2f %n", -23.3333);
		// Temperatur 4:		
				System.out.printf("+0%11s", "|");
				System.out.printf("%10.2f %n", -17.7778);
		// Temperatur 5:	
				System.out.printf("+20%10s", "|");
				System.out.printf("%10.2f %n", -6.6667);
		// Temperatur 6:
				System.out.printf("+30%10s", "|");
				System.out.printf("%10.2f %n", -1.1111);
		
		// Datei wurde "Committed"
		 
	}

}
