﻿import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args) {
    	
       double zuZahlenderBetrag = fahrkartenbestellungErfassen("Zu zahlender Betrag (EURO): ");   
       double AnzahlTickets = fahrkartenbestellungErfassen("Anzahl Tickets: ");
       
       // Geldeinwurf
       // -----------
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, AnzahlTickets);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag, AnzahlTickets);
    
    }
    
    static double fahrkartenbestellungErfassen(String text) {
    	
    	System.out.print(text);
    	double zuZahlenderBetragUndAnzahlTickets = tastatur.nextDouble();
    	return zuZahlenderBetragUndAnzahlTickets;
    }
    
    static double fahrkartenBezahlen(double zuZahlenderBetrag1, double AnzahlTickets) {
    	double zuZahlenderBetrag = zuZahlenderBetrag1 * AnzahlTickets; 
    	double eingezahlterGesamtbetrag = 0.0;
    	 while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   double eingeworfeneMünze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMünze;
         }
    	 return eingezahlterGesamtbetrag;
    }
    
    static void fahrkartenAusgeben() {
        
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag1, double AnzahlTickets) {
        
    	double zuZahlenderBetrag = zuZahlenderBetrag1 * AnzahlTickets; 
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
        return rückgabebetrag;
    }
}