import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int summeBis = 0;
		int summe = 0;
		int zaehler = 0;
		
		System.out.println("Geben Sie eine Zahl ein: ");
		summeBis = sc.nextInt();
		
		//Kopfgesteuerte Schleife
		
		while(zaehler <= summeBis) {
			summe = summe + zaehler;
			zaehler++;
		}
		
		System.out.printf("Mit der kopfgesteuerten Schleife: %d", summe);

		summe = 0;
		zaehler = 0;
		
		//Fu�gesteuerte Schleife
		
		do {
			summe = summe + zaehler;
			zaehler = zaehler + 1;
		}while(zaehler <= summeBis);
		
		System.out.printf("\nMit der fu�gesteuerten Schleife: %d", summe);
			
		summe = 0;
		
		for(int i = 1; i <= summeBis; i++) {
			summe = summe + i;
		}
		
		System.out.printf("\nMit der Z�hlschleife: %d", summe);
			
	}

}
